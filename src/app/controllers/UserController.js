import User from '../models/User';

import Cache from '../../lib/Cache';

class UserController {
  async store(req, res) {
    const { email } = req.body;

    const alreadyExists = await User.findOne({ where: { email } });

    if (alreadyExists) {
      return res.status(400).json({ error: 'User already exists.' });
    }

    const { id, name, provider } = await User.create(req.body);

    if (provider) await Cache.invalidate('providers');

    return res.status(201).json({
      id,
      name,
      email,
      provider,
    });
  }

  async update(req, res) {
    const { email, oldPassword } = req.body;

    const user = await User.findByPk(req.userId);

    if (user.email !== email) {
      const alreadyExists = await User.findOne({ where: { email } });
      if (alreadyExists) {
        return res.status(400).json({ error: 'User already exists.' });
      }
    }

    if (oldPassword && !(await user.checkPassword(oldPassword))) {
      return res.status(401).json({ error: 'Password does not match.' });
    }

    const { id, name, provider } = await user.update(req.body);

    return res.status(200).json({
      id,
      name,
      email,
      provider,
    });
  }
}

export default new UserController();
