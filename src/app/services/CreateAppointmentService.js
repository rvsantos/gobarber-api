import { startOfHour, parseISO, isBefore, format } from 'date-fns';
import pt from 'date-fns/locale/pt';

import User from '../models/User';
import Appointment from '../models/Appointment';
import Notification from '../schemas/Notification';
import Cache from '../../lib/Cache';

class CreateAppointmentService {
  async run({ provider_id, user_id, date }) {
    if (user_id === provider_id) {
      const err = new Error('Provider not available');
      err.status = 400;
      throw err;
    }

    /*
     * Check if provider_id is a provider
     */
    const isProvider = await User.findOne({
      where: { id: provider_id, provider: true },
    });

    if (!isProvider) {
      const err = new Error('You can only create appointments with providers');
      err.status = 400;
      throw err;
    }

    const hourStart = startOfHour(parseISO(date));

    /**
     * Check for past date
     */
    if (isBefore(hourStart, new Date())) {
      const err = new Error('Past dates are not permitted');
      err.status = 400;
      throw err;
    }

    /**
     * Check date availability
     */
    const checkAvailabity = await Appointment.findOne({
      where: { provider_id, canceled_at: null, date: hourStart },
    });

    if (checkAvailabity) {
      const err = new Error('Appointment date is not available');
      err.status = 400;
      throw err;
    }

    const appointment = await Appointment.create({
      user_id,
      provider_id,
      date: hourStart,
    });

    /**
     * Notify appointment provider
     */
    const user = await User.findByPk(user_id);
    const formattedDate = format(hourStart, "dd 'de' MMMM', às' H:mm'h'", {
      locale: pt,
    });

    await Notification.create({
      content: `Novo agendamento ${user.name} para dia ${formattedDate}`,
      user: provider_id,
    });

    /**
     * Invalidate cache
     */
    await Cache.invalidatePrefix(`user:${user.id}:appointments`);

    return appointment;
  }
}

export default new CreateAppointmentService();
