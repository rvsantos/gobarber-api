import * as Yup from 'yup';

export default async (req, res, next) => {
  try {
    const schema = Yup.object().shape({
      name: Yup.string()
        .min(8)
        .required('Name is required'),
      email: Yup.string()
        .email()
        .required('Email is required'),
      password: Yup.string()
        .min(6)
        .required('Password is required'),
    });

    await schema.validate(req.body, { abortEarly: false });

    return next();
  } catch (err) {
    return res
      .status(400)
      .json({ error: 'Validations fails', messages: err.inner });
  }
};
